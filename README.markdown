indentect
=========

Diagnose indentation.

Installation
------------

```bash
make test
sudo make install
```

To install it in another directory:

```bash
make install PREFIX=/some/path
```

Usage
-----

See

```bash
indentect.sh -h
```
